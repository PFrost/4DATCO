#ifndef DATATYPES_H
#define DATATYPES_H

#include <QStandardItem>

namespace Data {
enum DataTypes : int {
	Aircraft = QStandardItem::UserType + 1
};
}


#endif // DATATYPES_H
