#include "mainwindow.h"

#include "radarscreencore.h"

#include <QDebug>
#include <QEvent>
#include <QStatusBar>
#include "angleframewidget.h"
#include "settings.h"
#include <QPalette>


const char *MOUSE_POS_LATLON_FORMAT = "%1 %2° %3' %4\", %5 %6° %7' %8\"";

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent),
	  m_radarScreenWidget(new RadarScreenWidget)
{
	setupStatusBar();
	setupWidgets();

	connect(&RadarScreenCore::inst().scene(), SIGNAL(mouseLatLonPosChanged(QPointF)), this, SLOT(setMouseLatLonDeg(QPointF)));
}

MainWindow::~MainWindow()
{

}

void MainWindow::setMouseLatLonDeg(const QPointF &mouseLatLonDeg)
{
	QString NS = mouseLatLonDeg.x() >= 0 ? "N" : "S";

	qreal absLatDeg = qAbs(mouseLatLonDeg.x());

	int latDeg = (int)absLatDeg;
	int latMin = (int)((absLatDeg - qreal(latDeg))*60);
	float latSec = (absLatDeg - qreal(latDeg) - qreal(latMin) / 60.0) * 3600.f;

	QString EW = mouseLatLonDeg.y() >= 0 ? "E" : "W";

	qreal absLonDeg = qAbs(mouseLatLonDeg.y());

	int lonDeg = (int) absLonDeg;
	int lonMin = (int)((absLonDeg - qreal(lonDeg))*60.0);
	float lonSec = (absLonDeg - qreal(lonDeg) - qreal(lonMin) / 60.0) * 3600.f;

	m_statusBarMousePos = QString(MOUSE_POS_LATLON_FORMAT)
			.arg(NS)
			.arg(latDeg)
			.arg(latMin)
			.arg(latSec,0,'g',2)
			.arg(EW)
			.arg(lonDeg)
			.arg(lonMin)
			.arg(lonSec,0,'g',2);

	updateStatusBar();
}

void MainWindow::updateStatusBar()
{
	QStatusBar *statusBar = this->statusBar();
	if(m_statusBarMessage.isEmpty()){
		statusBar->showMessage(m_statusBarMousePos);
	}
	else {
		statusBar->showMessage(m_statusBarMessage);
	}

}

//bool MainWindow::eventFilter(QObject *watched, QEvent *event)
//{
//	qDebug() << watched->objectName();
//	event->accept();
//}

//void MainWindow::paintEvent(QPaintEvent *paintEvent)
//{

////	foreach(QObject *objectP, children()){
////		foreach(QObject *object, objectP->children()){
////			if(object->objectName() == "RulerBars"){
////				RulerBars *rulerbars = (RulerBars*)object;
////				rulerbars->paint(paintEvent);
////			}
////		}
////	}


//}

QString MainWindow::statusBarMessage() const
{
	return m_statusBarMessage;
}

void MainWindow::setStatusBarMessage(const QString &statusBarMessage)
{
	m_statusBarMessage = statusBarMessage;
}

void MainWindow::setupStatusBar()
{
	QStatusBar *statusBar = this->statusBar();
	statusBar->setVisible(true);
}

void MainWindow::setupWidgets()
{
	this->setCentralWidget(m_radarScreenWidget.data());

	QRect smallerGeometry = centralWidget()->geometry();
	smallerGeometry.moveBottom(statusBar()->height());

	this->centralWidget()->setGeometry(smallerGeometry);
	Settings &settings = RadarScreenCore::inst().settings();

	QPalette pal = palette();
	pal.setColor(QPalette::Background, QColor(settings.value("color::radarscreen::background").toString()));
	setPalette(pal);
}
