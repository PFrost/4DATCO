#ifndef PROJECTIONABSTRACT_H
#define PROJECTIONABSTRACT_H

#include <QPointF>
#include <QSharedPointer>

#include "plugininterface_global.h"
#include "latlon.h"


class PLUGININTERFACESHARED_EXPORT ProjectionAbstract
{
public:
	virtual ~ProjectionAbstract() = default;

	virtual QPointF convertToXYFromRad(LatLon latLon_rad) = 0;
	virtual QPointF convertToXYFromDeg(LatLon latLon_deg) = 0;

	virtual LatLon convertToLatLon_Deg(QPointF xy) = 0;
	virtual LatLon convertToLatLon_Rad(QPointF xy) = 0;

	virtual QRectF convertToXYRect(LatLon topLeftDeg, LatLon bottomRightDeg) = 0;

	virtual QPointF origin() const = 0;
	virtual void setOrigin(const QPointF &origin) = 0;

	virtual qreal scale() const = 0;
	virtual void setScale(const qreal &scale, const QPointF &xy = QPointF()) = 0;
};

#endif // PROJECTIONABSTRACT_H
