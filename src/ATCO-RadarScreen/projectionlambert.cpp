#include "projectionlambert.h"

#include "conversionparameter.h"

#include <QDebug>
#include <GeographicLib/LambertConformalConic.hpp>
#include <exception>
using namespace std;

const QPointF DEFAULT_ORIGIN_LATLON_DEG = QPointF(50.0+2.0/60.0, 8.0+34.0/60.0+14.0/3600.0); //EDDF Coordinates

ProjectionLambert::ProjectionLambert():
	m_origin(DEFAULT_ORIGIN_LATLON_DEG),
	m_scale(1.0)
{
	setLambertParameter(LambertParameter());
}

QPointF ProjectionLambert::convertToXYFromRad(LatLon latLon_rad)
{
	return convertToLatLon_Deg(latLon_rad * RAD2DEG);
}

QPointF ProjectionLambert::convertToXYFromDeg(LatLon latLon_deg)
{
		if(!m_geoLambertPtr){
			return QPointF();
		}

		qreal x,y;

		try {
			m_geoLambertPtr.data()->Forward(m_origin.y(), latLon_deg.lat(), latLon_deg.lon(), x, y);
			return QPointF(x, -y);
		}
		catch (const exception& e) {
			qWarning() << Q_FUNC_INFO << e.what();
			return QPointF();
		}

	return QPointF();
}

LatLon ProjectionLambert::convertToLatLon_Deg(QPointF xy)
{
	if(!m_geoLambertPtr){
		return QPointF();
	}

	qreal lat,lon;

	try {
		m_geoLambertPtr.data()->Reverse(m_origin.y(), xy.x(), -xy.y(), lat, lon);
		return LatLon(lat, lon);
	}
	catch (const exception& e) {
		qWarning() << "Caught exception: " << Q_FUNC_INFO << e.what();
		return LatLon();
	}

	return LatLon();
}

LatLon ProjectionLambert::convertToLatLon_Rad(QPointF xy)
{
	return convertToLatLon_Deg(xy) * DEG2RAD;
}

QRectF ProjectionLambert::convertToXYRect(LatLon topLeftDeg, LatLon bottomRightDeg)
{
	return QRectF(convertToXYFromDeg(topLeftDeg), convertToXYFromDeg(bottomRightDeg));
}

qreal ProjectionLambert::scale() const
{
	return m_scale;
}

void ProjectionLambert::setScale(const qreal &scale, const QPointF &xy)
{
	Q_UNUSED(xy);
	m_scale = scale;

	setLambertParameter(m_lambertParameter);
}

void ProjectionLambert::operator *=(qreal zoomFactor)
{
	m_scale *= zoomFactor;
}

QPointF ProjectionLambert::origin() const
{
	return m_origin;
}

void ProjectionLambert::setOrigin(const QPointF &origin)
{
	m_origin = origin;
}

ProjectionLambert::LambertParameter ProjectionLambert::lambertParameter() const
{
	return m_lambertParameter;
}

void ProjectionLambert::setLambertParameter(const LambertParameter &lambertParameter)
{
	m_lambertParameter = lambertParameter;

	try{
		m_geoLambertPtr = QSharedPointer<GeographicLib::LambertConformalConic>(new GeographicLib::LambertConformalConic(
																				   GeographicLib::Constants::WGS84_a(),
																				   GeographicLib::Constants::WGS84_f(),
																				   m_lambertParameter.lat1_deg,
																				   m_lambertParameter.lat2_deg,
																				   m_scale));
	}
	catch(const exception& e) {
		qWarning() << "Caught exception: " << Q_FUNC_INFO << e.what();
	}
}
