#include "angleframewidget.h"

#include <QtMath>
#include <QDebug>
#include <QPaintEvent>
#include <QPainter>
#include "radarscreencore.h"
#include "settings.h"


const int FRAME_WIDTH = 20;
const qreal LABEL_HEIGHT = 8.0;

AngleFrameWidget::AngleFrameWidget(QWidget *parent):QWidget(parent)
{
	setObjectName("RulerBars");

	Settings &settings = RadarScreenCore::inst().settings();//.value settings("color.json");

	m_backgroundColor = QColor(settings.value("color::radarscreen::background").toString());
	m_labelColor = QColor(settings.value("color::radarscreen::angleframe::label").toString());
	m_lineColor = QColor(settings.value("color::radarscreen::angleframe::line").toString());
}

void AngleFrameWidget::paintEvent(QPaintEvent *event)
{
	QWidget::paintEvent(event);

	QPainter painter(this);

	qreal h_2 = geometry().height() / 2.0;
	qreal w_2 = geometry().width() / 2.0;

	qreal longestSide = qSqrt(w_2 * w_2 + h_2 * h_2);

	QPointF origin(w_2, h_2);

	QPointF innerTopLeft(FRAME_WIDTH, FRAME_WIDTH);
	QPointF innerBottomRight(geometry().width() - FRAME_WIDTH, geometry().height() - FRAME_WIDTH);
	QRectF innerRect(innerTopLeft, innerBottomRight);

	QLineF innerTopSide(innerRect.topLeft(), innerRect.topRight());
	QLineF innerBottomSide(innerRect.bottomLeft(), innerRect.bottomRight());
	QLineF innerLeftSide(innerRect.topLeft(), innerRect.bottomLeft());
	QLineF innerRightSide(innerRect.topRight(), innerRect.bottomRight());

	QLineF outerTopSide(geometry().topLeft(), geometry().topRight());
	QLineF outerBottomSide(geometry().bottomLeft(), geometry().bottomRight());
	QLineF outerLeftSide(geometry().topLeft(), geometry().bottomLeft());
	QLineF outerRightSide(geometry().topRight(), geometry().bottomRight());

	QLineF tempLine(origin, QPointF(origin.x(), origin.y() + longestSide));

	QPointF innerPoint, outerPoint;

	QFont font;
	font.setPixelSize(LABEL_HEIGHT);
	painter.setFont(font);

	for(qreal angle = 10; angle <= 360 + 0.00001; angle += 10){

		tempLine.setAngle(-angle + 90);

		if(tempLine.intersect(outerTopSide, &outerPoint) != QLineF::IntersectType::BoundedIntersection ||
				tempLine.intersect(innerTopSide, &innerPoint)!= QLineF::IntersectType::BoundedIntersection) {
			if(tempLine.intersect(outerRightSide, &outerPoint) != QLineF::IntersectType::BoundedIntersection ||
					tempLine.intersect(innerRightSide, &innerPoint)!= QLineF::IntersectType::BoundedIntersection) {
				if(tempLine.intersect(outerBottomSide, &outerPoint) != QLineF::IntersectType::BoundedIntersection ||
						tempLine.intersect(innerBottomSide, &innerPoint)!= QLineF::IntersectType::BoundedIntersection) {
					if(tempLine.intersect(outerLeftSide, &outerPoint) != QLineF::IntersectType::BoundedIntersection ||
							tempLine.intersect(innerLeftSide, &innerPoint)!= QLineF::IntersectType::BoundedIntersection) {
						if(angle <= 90 || angle > 270){
							tempLine.intersect(outerTopSide, &outerPoint);
							tempLine.intersect(innerTopSide, &innerPoint);
						}
						else {
							tempLine.intersect(outerBottomSide, &outerPoint);
							tempLine.intersect(innerBottomSide, &innerPoint);
						}
					}
				}
			}
		}
		painter.setPen(m_lineColor);
		painter.drawLine(innerPoint, outerPoint);

		QRectF labelRect(innerPoint.x() - LABEL_HEIGHT * 1.5, innerPoint.y() - LABEL_HEIGHT / 2.0, LABEL_HEIGHT * 3.0, LABEL_HEIGHT);

		painter.setBrush(QBrush(m_backgroundColor));
		painter.setPen(Qt::NoPen);
		painter.drawRect(labelRect);

		painter.setPen(m_labelColor);
		painter.drawText(labelRect, Qt::AlignCenter, QString::number(angle).append("°"));
	}

}

