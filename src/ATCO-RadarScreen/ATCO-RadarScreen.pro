#-------------------------------------------------
#
# Project created by QtCreator 2018-06-25T21:55:58
#
#-------------------------------------------------

!exists(../Libraries.pri) {
      error( "No Libraries.pri file found ($${PWD}/../Libraries.pri). Use Libraries.pri.template to create the file Libraries.pri and fill in all required Libraries!" )
}

include(../Libraries.pri)

include(../Commons.pri)


QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ATCO-RadarScreen
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    conversionparameter.cpp \
    settings.cpp \
    latlon.cpp \
    radarscreenscene.cpp \
    radarscreenview.cpp \
    radarscreenwidget.cpp \
    angleframewidget.cpp \
    radarscreencore.cpp \
    radarscreenplugin.cpp \
    projectionlambert.cpp \
    datahandler.cpp \
    Data/aircraft.cpp

HEADERS += \
        mainwindow.h \
    conversionparameter.h \
    settings.h \
    latlon.h \
    radarscreenscene.h \
    radarscreenview.h \
    radarscreenwidget.h \
    angleframewidget.h \
    radarscreencore.h \
    plugininterface_global.h \
    radarscreenplugin.h \
    projectionabstract.h \
    projectionlambert.h \
    datahandler.h \
    Data/aircraft.h \
    Data/datatypes.h


CONFIG(release, debug|release): LIBS += $$GEOGRAPHIC_LIB_RELEASE $$DDS_LIB_RELEASE
CONFIG(debug, debug|release): LIBS += $$GEOGRAPHIC_LIB_DEBUG $$DDS_LIB_DEBUG

DISTFILES += \
    ../../res/style.qss \
    ../../res/json/color.json \
    ../../res/json/settings.json


win32 {
    PWD_WIN = $${PWD}
    PWD_WIN ~= s,/,\\,g

    QMAKE_POST_LINK += $$quote(mkdir DestFolder)
    QMAKE_POST_LINK += $$quote(xcopy $${PWD_WIN}\\$${DISTFILES} $${OUT_PWD_WIN}\\$${DISTFILES} /E)
    QMAKE_CLEAN += /s /f /q $$DISTFILES && rd /s /q $$DISTFILES
}

unix:!macx {
    QMAKE_POST_LINK += $$quote(cp -rf $${PWD}/$${DISTFILES} $${OUT_PWD})
}

macx: {
    QMAKE_POST_LINK += $$quote(cp -rf $${PWD}/$${DISTFILES} $${OUT_PWD}/ATCO-RadarScreen.app/Contents/MacOS)
    QMAKE_CLEAN += -r $${OUT_PWD}/ATCO-RadarScreen.app
}




