#ifndef LATLON_H
#define LATLON_H

#include "plugininterface_global.h"

#include <QMetaType>
#include <QPointF>
#include <QLineF>


class PLUGININTERFACESHARED_EXPORT LatLon : public QPointF
{
public:
	LatLon();
	LatLon(qreal lat, qreal lon);
	LatLon(const QPointF &point);
	qreal lat() const;
	void setLat(qreal lat);
	qreal lon() const;
	void setLon(qreal lon);
};

Q_DECLARE_METATYPE(LatLon)


class PLUGININTERFACESHARED_EXPORT LatLonLine : public QLineF
{
public:
	LatLonLine();
	LatLonLine(const LatLon &p1, const LatLon &p2);

	LatLon p1LatLon();
	void setP1LatLon(const LatLon &p1);

	LatLon p2LatLon();
	void setP2LatLon(const LatLon &p2);

	qreal lat1();
	qreal lat2();

	qreal lon1();
	qreal lon2();
};

Q_DECLARE_METATYPE(LatLonLine)

#endif // LATLON_H
