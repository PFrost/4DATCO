#ifndef RADARSCREENSCENE_H
#define RADARSCREENSCENE_H

#include <QGraphicsScene>
#include <QSharedPointer>
#include <QHash>

#include "plugininterface_global.h"

class ProjectionAbstract;
class RadarScreenCore;
class QMouseEvent;

class PLUGININTERFACESHARED_EXPORT RadarScreenScene : public QGraphicsScene
{
	Q_OBJECT
public:
	explicit RadarScreenScene(QObject *parent = Q_NULLPTR);

	void recalculateItems();

	void addItemToMapItemGroup(QGraphicsItem *item);
	void removeItemFromScene(QGraphicsItem *item);

private:
	QGraphicsItemGroup *m_mapItemGroup;

protected:
	void mouseMoveEvent(QGraphicsSceneMouseEvent *event);

signals:
	void mouseLatLonPosChanged(const QPointF &mouseLatLon);

public slots:
};

#endif // RADARSCREENSCENE_H
