#ifndef DATAHANDLER_H
#define DATAHANDLER_H

#include "Data/aircraft.h"

#include <QObject>
#include <QHash>
#include <QSharedPointer>



class DataHandler : public QObject
{
	Q_OBJECT
public:
	explicit DataHandler(QObject *parent = nullptr);

	void addAircraft(const QString &callsign, Data::AircraftData aircraftData = Data::AircraftData());
	void removeAircraft(const QString &callsign);
	QList<QSharedPointer<Data::Aircraft> > allAircraft() const;
	QSharedPointer<Data::Aircraft> aircraft(const QString &callsign);

private:
	QHash<QString, QSharedPointer<Data::Aircraft> > m_aircrafts;

signals:

public slots:
};

#endif // DATAHANDLER_H
