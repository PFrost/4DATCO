#ifndef RADARSCREENVIEW_H
#define RADARSCREENVIEW_H

#include <QGraphicsView>
#include <QSharedPointer>

class RadarScreenView : public QGraphicsView
{
	Q_OBJECT
public:
	explicit RadarScreenView(QWidget *parent = Q_NULLPTR);

	void setInitialPosition();
	void setMapPosition(QRectF mapRectToFit);

private:
	qreal m_lastDemandedScale;
	qreal m_currentScale;

	QRectF m_currentVisibleRect;

protected:
	void resizeEvent(QResizeEvent *event);
	bool viewportEvent(QEvent *event);

public slots:
};

#endif // RADARSCREEN_H
