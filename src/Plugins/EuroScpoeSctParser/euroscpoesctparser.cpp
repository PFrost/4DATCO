#include "euroscpoesctparser.h"

#include "settings.h"
#include "radarscreenscene.h"
#include "sectordata.h"

#include <QGraphicsItemGroup>
#include <QGraphicsLineItem>
#include <QDebug>

EuroScpoeSctParser::EuroScpoeSctParser():
	m_core(Q_NULLPTR)
{

}

void EuroScpoeSctParser::initParser()
{
	Settings &settings = m_core->settings();
	m_sectorData = QSharedPointer<SectorData>::create();
	m_sectorData->parseDataFromSctFile(settings.value("settings::files::mapdatafile").toString());
}

void EuroScpoeSctParser::paintGeo()
{

	qDebug() << Q_FUNC_INFO;

	foreach (QGraphicsItemGroup* itemGroup, m_GeoItemGroups) {
		m_core->scene().removeItemFromScene(itemGroup);
		delete itemGroup;
	}

	m_GeoItemGroups.clear();

	foreach(const QString &geoKey, m_sectorData->geoParts().keys()){
		DataGeo geo = m_sectorData->geoParts()[geoKey];

		QGraphicsItemGroup *itemGroup = new QGraphicsItemGroup;

		foreach(LatLonLine line, geo.lineList){

			QPointF p1 = m_core->mapProjection().convertToXYFromDeg(line.p1LatLon());
			QPointF p2 = m_core->mapProjection().convertToXYFromDeg(line.p2LatLon());
			QGraphicsLineItem *lineItem = new QGraphicsLineItem(QLineF(p1,p2), itemGroup);
			QPen pen = QPen(Qt::white);
			pen.setCosmetic(true);
			lineItem->setPen(pen);


		}
		m_GeoItemGroups.insert(geoKey, itemGroup);
		m_core->scene().addItemToMapItemGroup(itemGroup);
	}

	m_core->scene().recalculateItems();
}

void EuroScpoeSctParser::receiveCoreModule(RadarScreenCore *core)
{
	m_core = core;
	if(!m_core){
		return;
	}

	initParser();
	paintGeo();
}

PluginMetaData EuroScpoeSctParser::pluginMetaData() const
{
	return PluginMetaData("EuroScpoeSctParser", "Paul Frost", "Plugin draws objects of a Euroscope sct file.", "0.1");
}
