#ifndef CONVERSIONPARAMETER_H
#define CONVERSIONPARAMETER_H

#include <QtGlobal>

//Length
extern const qreal M2NM;
extern const qreal NM2M;

//Angular
extern const qreal DEG2RAD;
extern const qreal RAD2DEG;


#endif // CONVERSIONPARAMETER_H
