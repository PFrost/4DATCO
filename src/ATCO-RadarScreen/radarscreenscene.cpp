#include "radarscreenscene.h"
#include "radarscreencore.h"

#include "projectionabstract.h"
#include "conversionparameter.h"
#include "settings.h"

#include <QDebug>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsItemGroup>
#include <QGraphicsLineItem>

RadarScreenScene::RadarScreenScene(QObject *parent): QGraphicsScene(parent),
	m_mapItemGroup(new QGraphicsItemGroup)
{
	this->setObjectName("RadarScreenScene");
	this->addItem(m_mapItemGroup);
}

void RadarScreenScene::recalculateItems()
{
	QRectF mapBounding = m_mapItemGroup->boundingRect();

	mapBounding.adjust(-mapBounding.width(), -mapBounding.height(), mapBounding.width(), mapBounding.height());

	setSceneRect(mapBounding);
}

void RadarScreenScene::addItemToMapItemGroup(QGraphicsItem *item)
{
	m_mapItemGroup->addToGroup(item);
}

void RadarScreenScene::removeItemFromScene(QGraphicsItem *item)
{
	this->removeItem(item);
}

void RadarScreenScene::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
	QGraphicsScene::mouseMoveEvent(event);

//	qDebug() << event->scenePos();

	emit mouseLatLonPosChanged(RadarScreenCore::inst().mapProjection().convertToLatLon_Deg(event->scenePos()));
}
