#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSharedPointer>
#include "radarscreenwidget.h"

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow(QWidget *parent = 0);
	~MainWindow();

	QString statusBarMessage() const;
	void setStatusBarMessage(const QString &statusBarMessage);

public slots:
	void setMouseLatLonDeg(const QPointF &mouseLatLonDeg);
	void updateStatusBar();

protected:

private:
	QSharedPointer<RadarScreenWidget> m_radarScreenWidget;

	QString m_statusBarMessage;
	QString m_statusBarMousePos;

	void setupStatusBar();
	void setupWidgets();
};

#endif // MAINWINDOW_H
