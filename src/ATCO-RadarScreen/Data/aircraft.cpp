#include "aircraft.h"
#include "datatypes.h"

using namespace Data;

Aircraft::Aircraft(const QString &callsign,QObject *parent) : QObject(parent),
	m_callsign(callsign)
{
}

int Aircraft::type() const
{
	return DataTypes::Aircraft;
}

LatLon Aircraft::position_Deg() const
{
	return LatLon(m_aircraftData.lat_Deg, m_aircraftData.lon_Deg);
}

void Aircraft::setPosition(const LatLon &pos_Deg)
{
	m_aircraftData.lat_Deg = pos_Deg.lat();
	m_aircraftData.lon_Deg = pos_Deg.lon();
}

qreal Aircraft::altitudeMSL_Ft() const
{
	return m_aircraftData.altitude_MSL_Ft;
}

void Aircraft::setAltitudeMSL(const qreal &altMSL_Ft)
{
	m_aircraftData.altitude_MSL_Ft = altMSL_Ft;
}

qreal Aircraft::altitudeAGL_Ft() const
{
	return m_aircraftData.altitude_AGL_Ft;
}

void Aircraft::setAltitudeAGL(const qreal &altAGL_Ft)
{
	m_aircraftData.altitude_AGL_Ft = altAGL_Ft;
}

qreal Aircraft::verticalSpeed_FtMin() const
{
	return m_aircraftData.verticalSpeed_FtMin;
}

void Aircraft::setVerticalSpeed(const qreal &verticalSpeed_FtMin)
{
	m_aircraftData.verticalSpeed_FtMin = verticalSpeed_FtMin;
}

qreal Aircraft::vCAS_Kt() const
{
	return m_aircraftData.vCAS_Kt;
}

void Aircraft::setVCAS(const qreal &vCAS_Kt)
{
	m_aircraftData.vCAS_Kt = vCAS_Kt;
}

qreal Aircraft::vGS_Kt() const
{
	return m_aircraftData.vGS_Kt;
}

void Aircraft::setVGS(const qreal &vGS_Kt)
{
	m_aircraftData.vGS_Kt = vGS_Kt;
}

QString Aircraft::callsign() const
{
	return m_callsign;
}

void Aircraft::setAircraftData(const AircraftData &aircraftData)
{
	m_aircraftData = aircraftData;
}
