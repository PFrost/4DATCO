#include "ddsinterface.h"

#include <QTimer>
#include <QDebug>
#include <iostream>

DDSInterface::DDSInterface(QObject *parent) : QObject(parent),
	m_participant(0),
	m_topic(m_participant, "Trafficsimaircraft"),
	m_dataReader(rti::sub::implicit_subscriber(m_participant), m_topic)

{
	QTimer *timer = new QTimer(this);
	timer->setSingleShot(false);
	timer->start(200);
	connect(timer, SIGNAL(timeout()),this,SLOT(lookForData()));
}

void DDSInterface::lookForData()
{

	LoanedSamples<dds::core::StringTopicType> samples = m_dataReader.take();


	if (samples.length() == 0) {
		qDebug() <<  "No samples" ;
	}
	else{
		for (auto sample : samples) {
			if (sample.info().valid()) {
				std::cout << sample.data() << std::endl;
			}
		}
	}
}
