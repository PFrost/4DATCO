#ifndef EUROSCPOESCTPARSER_H
#define EUROSCPOESCTPARSER_H

#include "euroscpoesctparser_global.h"
#include "radarscreencore.h"
#include "radarscreenplugin.h"

#include <QObject>
#include <QtPlugin>
#include <QSharedPointer>
#include <QHash>

class QGraphicsItemGroup;
class SectorData;

class EUROSCPOESCTPARSERSHARED_EXPORT EuroScpoeSctParser : public QObject, public RadarScreenPlugin
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "paulfrost.ATCO-RadarScreen.Plugin.EuroScpoeSctParser")
	Q_INTERFACES(RadarScreenPlugin)

public:
	EuroScpoeSctParser();
	void receiveCoreModule(RadarScreenCore *core);
	PluginMetaData pluginMetaData() const;

private:
	RadarScreenCore *m_core;

	void initParser();
	void paintGeo();

	QSharedPointer<SectorData> m_sectorData;
	QHash<QString, QGraphicsItemGroup* > m_GeoItemGroups;
};

#endif // EUROSCPOESCTPARSER_H
