#-------------------------------------------------
#
# Project created by QtCreator 2018-07-21T22:48:01
#
#-------------------------------------------------


include(../../Commons.pri)

QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PluginInterface
TEMPLATE = lib
CONFIG += staticlib

DEFINES += PLUGININTERFACE_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    ../../ATCO-RadarScreen/latlon.cpp \
    ../../ATCO-RadarScreen/radarscreencore.cpp \
    ../../ATCO-RadarScreen/radarscreenplugin.cpp \
    ../../ATCO-RadarScreen/radarscreenscene.cpp \
    ../../ATCO-RadarScreen/settings.cpp

HEADERS += \
    ../../ATCO-RadarScreen/latlon.h \
    ../../ATCO-RadarScreen/plugininterface_global.h \
    ../../ATCO-RadarScreen/radarscreencore.h \
    ../../ATCO-RadarScreen/radarscreenplugin.h \
    ../../ATCO-RadarScreen/radarscreenscene.h \
    ../../ATCO-RadarScreen/projectionabstract.h \
    ../../ATCO-RadarScreen/settings.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

win32:VERSION = 0.1.0.0 # major.minor.patch.build
else:VERSION = 0.1.0    # major.minor.patch

DESTDIR = lib

unix:!macx {
    APPBUNDLEPATH=$$shadowed($$PWD)/../../$${RADARSCREEN_APP_TARGET_NAME}
    FULLTARGETFILEPATH = $${DESTDIR}/*

    QMAKE_POST_LINK += $$quote(cp -rf $${FULLTARGETFILEPATH} $${APPBUNDLEPATH})
    QMAKE_CLEAN += -r $${APPBUNDLEPATH}/lib$${TARGET}*.so
}

macx {
    APPBUNDLEPATH=$$shadowed($$PWD)/../../$${RADARSCREEN_APP_TARGET_NAME}/$${RADARSCREEN_APP_TARGET_NAME}.app/Contents/MacOS
    FULLTARGETFILEPATH = $${DESTDIR}/*

    QMAKE_POST_LINK += $$quote(cp -rf $${FULLTARGETFILEPATH} $${APPBUNDLEPATH})
    QMAKE_CLEAN += -r $${APPBUNDLEPATH}/lib$${TARGET}*.dylib
}
