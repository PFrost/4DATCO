#ifndef RADARSCREENPLUGIN_H
#define RADARSCREENPLUGIN_H

#include "plugininterface_global.h"
#include <QObject>

class RadarScreenCore;

struct PluginMetaData
{
	PluginMetaData(const QString &name, const QString &author, const QString &description, const QString &version):
		name(name),
		author(author),
		description(description),
		version(version){}
	QString name;
	QString author;
	QString description;
	QString version;
};

class PLUGININTERFACESHARED_EXPORT RadarScreenPlugin
{

public:
	virtual ~RadarScreenPlugin() = default;

	virtual void receiveCoreModule(RadarScreenCore *core) = 0;

	virtual PluginMetaData pluginMetaData() const = 0;
};

Q_DECLARE_INTERFACE(RadarScreenPlugin, "paulfrost.ATCO-RadarScreen.RadarScreenPlugin")


#endif // RADARSCREENPLUGIN_H
