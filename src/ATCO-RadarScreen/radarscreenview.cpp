#include "radarscreenview.h"


#include <QDebug>
#include <QMouseEvent>
#include <QScrollBar>
#include <QTouchEvent>

#include "radarscreencore.h"

const qreal ZOOM_THRESHOLD = 0.4;
const qreal ZOOM_MIN_DEVIATION = 0.05;

RadarScreenView::RadarScreenView(QWidget *parent) : QGraphicsView(parent),
	m_lastDemandedScale(1.0),
	m_currentScale(1.0)
{
	this->setObjectName("RadarScreenView");

	this->setMouseTracking(true);

	this->viewport()->setAttribute(Qt::WA_AcceptTouchEvents);
}

void RadarScreenView::setMapPosition(QRectF mapRectToFit)
{
	QRectF screenRect =this->geometry();

	qreal ratioScreen = screenRect.width()/screenRect.height();
	qreal ratioSectionToShow = mapRectToFit.width()/mapRectToFit.height();

	bool expandWidth = ratioSectionToShow >= ratioScreen;

	m_currentScale = expandWidth ?	screenRect.width() / mapRectToFit.width():
									screenRect.height() / mapRectToFit.height();

	qreal newWidth;
	qreal newHeight;

	qreal adjustX = 0.0;
	qreal adjustY = 0.0;

	if(expandWidth){
		newWidth = mapRectToFit.width();
		newHeight = newWidth / ratioScreen;
	}
	else {
		newHeight = mapRectToFit.height();
		newWidth = newHeight * ratioScreen;
	}

	adjustX = (newWidth - mapRectToFit.width())/2.0;
	adjustY = (newHeight - mapRectToFit.height())/2.0;

	mapRectToFit.adjust(-adjustX, -adjustY, adjustX, adjustY);

	setTransform(QTransform().scale(m_currentScale,m_currentScale));
	this->centerOn(mapRectToFit.center());
}

void RadarScreenView::resizeEvent(QResizeEvent *event)
{
	QGraphicsView::resizeEvent(event);

////	QRectF visibleScreenCoords =  mapToScene(viewport()->geometry()).boundingRect();

	QPointF tl = m_currentVisibleRect.topLeft();
	QPointF br = m_currentVisibleRect.bottomRight();

	setMapPosition(QRectF(tl,br));
//	centerOn(m_currentVisibleRect.center());
}

void RadarScreenView::setInitialPosition()
{
	RadarScreenCore &core = RadarScreenCore::inst();

	LatLon sceneTopLeftDeg = LatLon(core.settings().value("settings::radarscreen::range::top").toReal(),
									core.settings().value("settings::radarscreen::range::left").toReal());
	LatLon sceneBottomRightDeg = LatLon(core.settings().value("settings::radarscreen::range::bottom").toReal(),
									core.settings().value("settings::radarscreen::range::right").toReal());

	QRectF sectionToShowRect = core.mapProjection().convertToXYRect(sceneTopLeftDeg, sceneBottomRightDeg);

	setMapPosition(sectionToShowRect);

	m_currentVisibleRect = sectionToShowRect;
}

bool RadarScreenView::viewportEvent(QEvent *event)
{
	switch (event->type()) {
	case QEvent::TouchBegin:
	case QEvent::TouchUpdate:
	case QEvent::TouchEnd:
	{
		QTouchEvent *touchEvent = static_cast<QTouchEvent *>(event);
		QList<QTouchEvent::TouchPoint> touchPoints = touchEvent->touchPoints();
		if (touchPoints.count() == 2) {
			// determine scale factor
			const QTouchEvent::TouchPoint &touchPoint0 = touchPoints.first();
			const QTouchEvent::TouchPoint &touchPoint1 = touchPoints.last();
			qreal scaleFactor =
					(qreal)(qRound(QLineF(touchPoint0.pos(), touchPoint1.pos()).length()))
					/ (qreal)(qRound(QLineF(touchPoint0.startPos(), touchPoint1.startPos()).length()));

			qreal demandedScale = m_currentScale * scaleFactor;

			if(	(scaleFactor <= 1.0 - ZOOM_THRESHOLD / 2.0 || // Distinguish between pinch and pan
					scaleFactor >= 1.0 + ZOOM_THRESHOLD )
					&&
				(demandedScale/m_lastDemandedScale >= 1.0 + ZOOM_MIN_DEVIATION || // Avoids jittering when fingers will be left on touch device
					demandedScale/m_lastDemandedScale <= 1.0 - ZOOM_MIN_DEVIATION)) {
				m_lastDemandedScale = demandedScale;
				setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
				setTransform(QTransform().scale(demandedScale,demandedScale));
			}

			if (touchEvent->touchPointStates() & Qt::TouchPointReleased) {
				m_currentScale = demandedScale;
			}
		}
		m_currentVisibleRect = mapToScene(viewport()->geometry()).boundingRect();
		return true;
	}
	default:
		break;
	}

	return QGraphicsView::viewportEvent(event);
}
