#include "latlon.h"

LatLon::LatLon():QPointF()
{
}

LatLon::LatLon(qreal lat, qreal lon) : QPointF(lat, lon)
{

}

LatLon::LatLon(const QPointF &point) : QPointF(point)
{

}

qreal LatLon::lat() const
{
	return x();
}

void LatLon::setLat(qreal lat)
{
	setX(lat);
}

qreal LatLon::lon() const
{
	return y();
}

void LatLon::setLon(qreal lon)
{
	setY(lon);
}


LatLonLine::LatLonLine():QLineF()
{

}

LatLonLine::LatLonLine(const LatLon &p1, const LatLon &p2):QLineF(p1, p2)
{

}

LatLon LatLonLine::p1LatLon()
{
	return LatLon(p1());
}

void LatLonLine::setP1LatLon(const LatLon &p1)
{
	setP1(p1);
}

LatLon LatLonLine::p2LatLon()
{
	return LatLon(p2());
}

void LatLonLine::setP2LatLon(const LatLon &p2)
{
	setP2(p2);
}

qreal LatLonLine::lat1()
{
	return x1();
}

qreal LatLonLine::lat2()
{
	return x2();
}

qreal LatLonLine::lon1()
{
	return y1();
}

qreal LatLonLine::lon2()
{
	return y2();
}
