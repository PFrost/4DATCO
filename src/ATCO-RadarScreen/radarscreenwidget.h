#ifndef RADARSCREENWIDGET_H
#define RADARSCREENWIDGET_H

#include <QWidget>
#include <QSharedPointer>

class RadarScreenView;
class AngleFrameWidget;

class RadarScreenWidget : public QWidget
{
	Q_OBJECT
public:
	explicit RadarScreenWidget(QWidget *parent = nullptr);

	QSharedPointer<RadarScreenView>view();

protected:
	void resizeEvent(QResizeEvent *event);

private:
	QSharedPointer<RadarScreenView> m_view;
	QSharedPointer<AngleFrameWidget> m_angleFrame;

	void resizeChildren();

signals:

public slots:

};

#endif // RADARSCREENWIDGET_H
