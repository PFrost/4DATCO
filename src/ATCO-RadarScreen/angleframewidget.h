#ifndef ANGLEFRAMEWIDGET_H
#define ANGLEFRAMEWIDGET_H

#include <QWidget>

class AngleFrameWidget : public QWidget
{
	Q_OBJECT
public:
	AngleFrameWidget(QWidget *parent = Q_NULLPTR);
protected:
	void paintEvent(QPaintEvent *event);

private:
	QColor m_backgroundColor;
	QColor m_labelColor;
	QColor m_lineColor;
};

#endif // ANGLEFRAME_H
