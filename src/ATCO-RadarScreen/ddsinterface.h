#ifndef DDSINTERFACE_H
#define DDSINTERFACE_H

#include <QObject>

#define RTI_UNIX
#include <dds/dds.hpp>

using namespace dds::core;
using namespace dds::core::status;
using namespace dds::domain;
using namespace dds::topic;
using namespace dds::sub;

class DDSInterface : public QObject
{
	Q_OBJECT
public:
	explicit DDSInterface(QObject *parent = nullptr);

private:
	DomainParticipant m_participant;
	Topic<StringTopicType> m_topic;
	DataReader<StringTopicType> m_dataReader;
signals:

public slots:
	void lookForData();
};

#endif // DDSINTERFACE_H
