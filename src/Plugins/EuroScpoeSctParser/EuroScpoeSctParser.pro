#-------------------------------------------------
#
# Project created by QtCreator 2018-07-31T21:50:58
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = EuroScpoeSctParser
TEMPLATE = lib

DEFINES += EUROSCPOESCTPARSER_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        euroscpoesctparser.cpp \
    sectordata.cpp

HEADERS += \
        euroscpoesctparser.h \
        euroscpoesctparser_global.h \ 
    sectordata.h

INCLUDEPATH += \
        ../../ATCO-RadarScreen

unix {
    target.path = /usr/lib
    INSTALLS += target
}

win32:VERSION = 0.1.0.0 # major.minor.patch.build
else:VERSION = 0.1.0    # major.minor.patch

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../PluginInterface/release/lib -lPluginInterface
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../PluginInterface/debug/lib -lPluginInterface
else:unix: LIBS += -L$$OUT_PWD/../PluginInterface/lib -lPluginInterface

INCLUDEPATH += $$PWD/../PluginInterface
DEPENDPATH += $$PWD/../PluginInterface
