#include "datahandler.h"

DataHandler::DataHandler(QObject *parent) : QObject(parent)
{

}

void DataHandler::addAircraft(const QString &callsign, Data::AircraftData aircraftData)
{
	QSharedPointer<Data::Aircraft> aircraft = QSharedPointer<Data::Aircraft>::create(callsign);
	aircraft->setAircraftData(aircraftData);

	m_aircrafts.insert(callsign, aircraft);
}

void DataHandler::removeAircraft(const QString &callsign)
{
	m_aircrafts.remove(callsign);
}

QList<QSharedPointer<Data::Aircraft> > DataHandler::allAircraft() const
{
	return m_aircrafts.values();
}

QSharedPointer<Data::Aircraft> DataHandler::aircraft(const QString &callsign)
{
	return m_aircrafts.value(callsign, QSharedPointer<Data::Aircraft>(Q_NULLPTR));
}


