#include "conversionparameter.h"

const qreal M2NM = 0.000539957;
const qreal NM2M = 1852;

const qreal DEG2RAD = 0.0174533;
const qreal RAD2DEG = 57.2958;
