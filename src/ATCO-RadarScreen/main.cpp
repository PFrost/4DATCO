#include "radarscreencore.h"
#include "projectionlambert.h"
#include "mainwindow.h"

#include <QApplication>
#include <QSharedPointer>
#include <QDebug>
#include <QFile>


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);

	QString styleFileName = "./style.qss";

	for(int i = 0; i < argc; i++){
		if(QString(argv[i]) == "--style"
				|| QString(argv[i]) == "-s"){
			if(i+1 < argc){
				styleFileName = argv[i + 1];
			}
			else {
				qFatal("Input parameter missing!");
			}
		}
	}

	QFile styleSheetFile(styleFileName);

	if(styleSheetFile.open(QIODevice::ReadOnly)){
		qDebug() << "Loading styleSheetFile:" << styleFileName;
		a.setStyleSheet(QString(styleSheetFile.readAll()));
	}

	QSharedPointer<ProjectionLambert> lambert(new ProjectionLambert);

	RadarScreenCore &core = RadarScreenCore::inst();
	core.setMapProjection(lambert.dynamicCast<ProjectionAbstract>());
	core.loadPlugins();

	MainWindow w;
	w.show();

	return a.exec();
}
