#include "sectordata.h"

#include <QFile>
#include <QDebug>
#include <QStringList>


SectorData::SectorData()
{

}

void SectorData::parseDataFromSctFile(const QString &fileName)
{
	QFile sctFile(fileName);
	if(sctFile.open(QIODevice::ReadOnly)){

		DataType currentType = None;

		while(!sctFile.atEnd()){
			QByteArray line = sctFile.readLine();

			if(line.isEmpty()){
				continue;
			}

			if(line[0] == '['){
				if(line.contains(QByteArray("[VOR]"))){
					currentType = DataType::VOR;
				}
				if(line.contains(QByteArray("[NDB]"))){
					currentType = DataType::NDB;
				}
				if(line.contains(QByteArray("[FIXES]"))){
					currentType = DataType::FIX;
				}
				if(line.contains(QByteArray("[HIGH AIRWAY]"))){
					currentType = DataType::UpperAirway;
				}
				if(line.contains(QByteArray("[AIRPORT]"))){
					currentType = DataType::Airport;
				}
				if(line.contains(QByteArray("[STAR]"))){
					currentType = DataType::Star;
				}
				if(line.contains(QByteArray("[ARTCC]"))){
					currentType = DataType::ARTCC;
				}
				if(line.contains(QByteArray("[ARTCC LOW]"))){
					currentType = DataType::ARTCCLow;
				}
				if(line.contains(QByteArray("[ARTCC HIGH]"))){
					currentType = DataType::ARTCCHigh;
				}
				if(line.contains(QByteArray("[GEO]"))){
					currentType = DataType::Geo;
				}
			}
			else {
				parseSctData(currentType, line);
			}
		}
	}
	else {
		qWarning() << "Could not open file: " << fileName;
	}
}

QHash<QString, DataGeo> SectorData::geoParts() const
{
    return m_geoParts;
}

QHash<QString, DataARTCC> SectorData::artccs() const
{
    return m_artccs;
}

QMultiHash<QString, DataAirway> SectorData::upperAirways() const
{
    return m_upperAirways;
}

QMultiHash<QString, DataVOR> SectorData::vors() const
{
    return m_vors;
}

QMultiHash<QString, DataNDB> SectorData::ndbs() const
{
    return m_ndbs;
}

QMultiHash<QString, DataFix> SectorData::fixes() const
{
    return m_fixes;
}

QMultiHash<QString, DataAirport> SectorData::airports() const
{
    return m_airports;
}

qreal coordSctStringToReal(const QString &coordString){
    qreal multiplier = (coordString[0] == 'N' ||
					   coordString[0] == 'E') ? 1.0 : -1.0;
	QStringList numberParts = coordString.mid(1).split(".");

	QString deg = numberParts.at(0);
	QString min = numberParts.at(1);
	QString sec = QString("%1.%2").arg(numberParts.at(2),numberParts.at(3));

	return multiplier * (deg.toDouble() + min.toDouble() / 60.0 + sec.toDouble() / 3600.0);
}

LatLon SectorData::parseSctLatLon(const QString &lat, const QString &lon)
{
	if(lat.isEmpty() || lon.isEmpty()){
		return LatLon();
	}

	return LatLon(coordSctStringToReal(lat), coordSctStringToReal(lon));
}

void SectorData::parseSctData(SectorData::DataType type, QByteArray &data)
{
	QString lineString = data;


	if(lineString.contains(";")){
		lineString.truncate(lineString.indexOf(";"));
	}

	if(lineString.contains("\r\n")){
		lineString.truncate(lineString.indexOf("\r\n"));
	}

	if(lineString.isEmpty()){
		return;
	}

	QStringList lineStrings = lineString.split("\t");

	switch (type) {
	case DataType::Airport:
	{
		lineStrings = lineStrings.first().split(" ");

		if(lineStrings.count() < 3){
			return;
		}

		DataAirport airportData;

		QString latCoord;
		QString lonCoord;
		foreach (QString str, lineStrings) {
			if(str.length() < 4){
				continue;
			}
			if(str.length() == 4){
				airportData.ident = str;
			}
			else if(str.at(0) == 'N' ||
					str.at(0) == 'S'){
				latCoord = str;
			}
			else if(str.at(0) == 'E' ||
					str.at(0) == 'W'){
				lonCoord = str;
			}
		}

		if(latCoord.isEmpty() || lonCoord.isEmpty()){
			return;
		}

		airportData.coord = parseSctLatLon(latCoord, lonCoord);
		m_airports.insert(airportData.ident,airportData);
		break;
	}
	case DataType::ARTCC:
	case DataType::ARTCCHigh:
	case DataType::ARTCCLow:
	{
		lineStrings = lineStrings.first().split(" ", QString::SkipEmptyParts);
		if(lineStrings.count() < 5){
			return;
		}

		DataARTCC artcc = m_artccs.value(lineStrings[0], DataARTCC(lineStrings[0]));
		artcc << LatLonLine(parseSctLatLon(lineStrings[1],lineStrings[2]), parseSctLatLon(lineStrings[3],lineStrings[4]));
		m_artccs.insert(artcc.ident, artcc);
		break;
	}
	case DataType::FIX:
	{
		if(lineStrings.count() < 3){
			return;
		}
		DataFix fixData(lineStrings[0],parseSctLatLon(lineStrings[1], lineStrings[2]));
		m_fixes.insert(fixData.ident, fixData);
		break;
	}
	case DataType::Geo:
	{

		lineStrings = lineStrings.first().split(" ", QString::SkipEmptyParts);
		if(lineStrings.count() < 5){
			return;
		}

		DataGeo geo = m_geoParts.value(lineStrings[4], DataGeo(lineStrings[4]));
		geo << LatLonLine(parseSctLatLon(lineStrings[0],lineStrings[1]), parseSctLatLon(lineStrings[2],lineStrings[3]));
		m_geoParts.insert(geo.type, geo);

		break;
	}
	case DataType::NDB:
	{
		if(lineStrings.count() < 4){
			return;
		}
		DataNDB ndbData(lineStrings[0],lineStrings[1],parseSctLatLon(lineStrings[2], lineStrings[3]));
		m_ndbs.insert(ndbData.ident, ndbData);
		break;
	}
	case DataType::Star:
	{
		return;
	}
	case DataType::UpperAirway:
	{
		if(lineStrings.count() < 5){
			return;
		}
		DataAirway airwayData(lineStrings[0], LatLonLine(parseSctLatLon(lineStrings[1], lineStrings[2]), parseSctLatLon(lineStrings[3], lineStrings[4])));
		m_upperAirways.insert(airwayData.ident, airwayData);
		break;
	}
	case DataType::VOR:
	{
		if(lineStrings.count() < 4){
			return;
		}
		DataVOR vorData(lineStrings[0],lineStrings[1],parseSctLatLon(lineStrings[2], lineStrings[3]));
		m_vors.insert(vorData.ident, vorData);
		break;
	}
	case DataType::None:
	{
		return;
	}
}
}
