#include "radarscreenwidget.h"

#include "radarscreenview.h"
#include "radarscreencore.h"
#include "angleframewidget.h"
#include "settings.h"

#include <QPalette>
#include <QDebug>

const int WIDGET_WIDTH = 400;
const int WIDGET_HEIGHT = 300;

RadarScreenWidget::RadarScreenWidget(QWidget *parent) : QWidget(parent)
{
	this->setObjectName("RadarScreenWidget");
	this->setMinimumSize(WIDGET_WIDTH,WIDGET_HEIGHT);

	m_angleFrame = QSharedPointer<AngleFrameWidget>(new AngleFrameWidget(this));
	m_view = QSharedPointer<RadarScreenView>(new RadarScreenView(this));

	m_view->setMinimumSize(WIDGET_WIDTH, WIDGET_HEIGHT);

	m_view->setScene(&RadarScreenCore::inst().scene());

	resizeChildren();

	m_view->setInitialPosition();
}

QSharedPointer<RadarScreenView> RadarScreenWidget::view()
{
	return m_view;
}

void RadarScreenWidget::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
	resizeChildren();
}

void RadarScreenWidget::resizeChildren()
{
	m_angleFrame->setGeometry(this->geometry());
	m_view->setGeometry(this->geometry());
}
