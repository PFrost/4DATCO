#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QVariant>
#include <QJsonDocument>
#include <QHash>
#include <QSharedPointer>

#include "plugininterface_global.h"

class PLUGININTERFACESHARED_EXPORT Settings : public QObject
{
	Q_OBJECT
public:
	explicit Settings(QObject *parent = nullptr);

	static const QString COLORFILENAME;

	QVariant value(const QString &key);

private:
	QJsonDocument m_jsonDocument;

	QByteArray loadSettingsFile(const QString &fileName);

	QHash<QString, QByteArray> m_jsonContainers;

signals:

public slots:
};

#endif // SETTINGS_H
