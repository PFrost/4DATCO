#ifndef SECTORDATA_H
#define SECTORDATA_H

#include "latlon.h"

#include <QString>
#include <QMultiHash>



struct DataVOR
{
	DataVOR(){}
	DataVOR(QString ident, QString frq, LatLon coord):
		ident(ident),
		frq(frq),
		coord(coord){}
	QString ident;
	QString frq;
	LatLon coord;
};

struct DataNDB
{
	DataNDB(){}
	DataNDB(QString ident, QString frq, LatLon coord):
		ident(ident),
		frq(frq),
		coord(coord){}
	QString ident;
	QString frq;
	LatLon coord;
};

struct DataFix
{
	DataFix(){}
	DataFix(QString ident, LatLon coord):
		ident(ident),
		coord(coord){}
	QString ident;
	LatLon coord;
	QString countryCode;
};

struct DataAirway
{
	DataAirway(){}
	DataAirway(QString ident, LatLonLine line):
		ident(ident),
		line(line){}
	QString ident;
	LatLonLine line;
};

struct DataAirport
{
	DataAirport(){}
	DataAirport(QString ident, LatLon coord):
		ident(ident),
		coord(coord){}
	QString ident;
	LatLon coord;
};

struct DataFIRLabel
{
	DataFIRLabel(){}
	DataFIRLabel(QString ident, LatLonLine line):
		ident(ident),
		line(line){}
	QString ident;
	LatLonLine line;
};

struct DataARTCC
{
	DataARTCC(){}
	DataARTCC(QString ident):
		ident(ident){}
	inline void operator <<(LatLonLine line){lineList.append(line);}
	QString ident;
	QList<LatLonLine> lineList;
};

struct DataGeo
{
	DataGeo(){}
	DataGeo(QString type):type(type){}
	inline void operator <<(LatLonLine line){lineList.append(line);}
	QString type;
	QList<LatLonLine> lineList;
};

class SectorData
{
public:
	enum DataType{
		None, VOR, NDB, FIX, UpperAirway, Airport, Star, ARTCC, ARTCCHigh, ARTCCLow, Geo
	};

	SectorData();

	void parseDataFromSctFile(const QString &fileName);

	QMultiHash<QString, DataVOR> vors() const;
	QMultiHash<QString, DataNDB> ndbs() const;
	QMultiHash<QString, DataFix> fixes() const;
	QMultiHash<QString, DataAirport> airports() const;
	QMultiHash<QString, DataAirway> upperAirways() const;
	QHash<QString, DataARTCC> artccs() const;
	QHash<QString, DataGeo> geoParts() const;

private:
	QMultiHash<QString, DataVOR> m_vors;
	QMultiHash<QString, DataNDB> m_ndbs;
	QMultiHash<QString, DataFix> m_fixes;
	QMultiHash<QString, DataAirport> m_airports;
	QMultiHash<QString, DataAirway> m_upperAirways;
	QHash<QString, DataARTCC> m_artccs;
	QHash<QString, DataGeo> m_geoParts;


	LatLon parseSctLatLon(const QString &lat, const QString &lon);
	void parseSctData(DataType type, QByteArray &data);
};


#endif // SECTORDATA_H
