#include "radarscreencore.h"

#include <QDebug>
#include <QDir>
#include <QPluginLoader>
#include <QWeakPointer>


#ifdef Q_OS_WIN
	#define OS_LIB_EXTENSION ".dll"
#elif defined(Q_OS_DARWIN)
	#define OS_LIB_EXTENSION ".dylib"
#elif defined(Q_OS_UNIX)
	#define OS_LIB_EXTENSION ".so"
#else
#error "This operating system is not yet supported. Add an OS specific lib extension definition."
#endif


RadarScreenCore &RadarScreenCore::inst()
{
	static RadarScreenCore instance;
	return instance;
}

ProjectionAbstract &RadarScreenCore::mapProjection()
{
	return *m_mapProjection;
}

RadarScreenScene &RadarScreenCore::scene()
{
	if(!m_scene){
		m_scene = QSharedPointer<RadarScreenScene>::create();
	}
	return *m_scene;
}

RadarScreenCore::RadarScreenCore()
{
}

void RadarScreenCore::loadPlugins()
{
	QDir pluginDir(settings().value("settings::directories::plugindir").toString());
	if(!pluginDir.exists()){
		return;
	}

	foreach (QString dir, pluginDir.entryList(QDir :: Dirs)) {
		if(dir == "." ||
				dir == ".." ||
				dir == "PluginInterface"){
			continue;
		}

		pluginDir.cd(dir);
		foreach (QString file, pluginDir.entryList(QDir :: Files)){
			if(file.contains(OS_LIB_EXTENSION)){
				qDebug() << "Trying to load plugin "<< file;

				QPluginLoader pluginLoader(pluginDir.absoluteFilePath(file));
				QObject *plugin = pluginLoader.instance();

				if (plugin) {
					RadarScreenPlugin *radarScreenPlugin = qobject_cast<RadarScreenPlugin *>(plugin);
					if (radarScreenPlugin){
						PluginMetaData pluginMetaData = radarScreenPlugin->pluginMetaData();

						qInfo() << QString("Plugin %1 (%2) loaded").arg(pluginMetaData.name, pluginMetaData.version);

						radarScreenPlugin->receiveCoreModule(this);
						m_registeredplugins.insert(pluginMetaData.name, QSharedPointer<RadarScreenPlugin>(radarScreenPlugin));
						break;
					}
					else {
						delete plugin;
					}
				}
				else{
					qWarning() << pluginLoader.errorString();
				}
			}
		}
		pluginDir.cdUp();
	}
}

Settings &RadarScreenCore::settings()
{
	if(!m_settings){
		m_settings = QSharedPointer<Settings>::create();
	}
	return *m_settings;
}

void RadarScreenCore::setMapProjection(const QSharedPointer<ProjectionAbstract> &mapProjection)
{
	m_mapProjection = mapProjection;
	scene().recalculateItems();
}
