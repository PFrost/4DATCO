#include "settings.h"

#include <QFile>
#include <QJsonObject>
#include <QJsonValue>
#include <QDebug>

const QString Settings::COLORFILENAME = "color.json";

Settings::Settings(QObject *parent) : QObject(parent)
{

}

QVariant Settings::value(const QString &key)
{
	QStringList keyHierarchy = key.split("::");

	QByteArray container = m_jsonContainers.value(keyHierarchy.first(),QByteArray());

	if(container.isEmpty()){
		container = loadSettingsFile(keyHierarchy.first());
	}

	keyHierarchy.removeFirst();

	QJsonDocument jsonDocument = QJsonDocument::fromJson(container);
	QJsonValue jsonValue = jsonDocument[keyHierarchy.first()];
	QJsonObject jsonObject;

	int index = 1;

	while (jsonValue.isObject() && index < keyHierarchy.count()){
		jsonObject = jsonValue.toObject();
		jsonValue = jsonObject[keyHierarchy[index]];

		index++;
	}

	if(index == keyHierarchy.count()){
		return jsonValue.toVariant();
	}

	return QVariant();
}

QByteArray Settings::loadSettingsFile(const QString &fileName)
{
	QString correctedFileName = fileName;

	if(!correctedFileName.contains(".json")){
		correctedFileName.append(".json");
	}

	QFile settingsFile(correctedFileName);

	QByteArray bArray;
	if(settingsFile.open(QIODevice::ReadOnly)){

		bArray = settingsFile.readAll();
		m_jsonContainers.insert(fileName, bArray);
	}

	settingsFile.close();
	return bArray;
}

