#ifndef AIRCRAFT_H
#define AIRCRAFT_H

#include <QStandardItem>
#include <QScopedPointer>
#include <latlon.h>

namespace Data {

#pragma pack(1)
struct AircraftData {
	AircraftData():
	lat_Deg(0.0),
	lon_Deg(0.0),
	altitude_MSL_Ft(0.0),
	altitude_AGL_Ft(0.0),
	verticalSpeed_FtMin(0.0),
	vCAS_Kt(0.0),
	vGS_Kt(0.0){}

	qreal lat_Deg;
	qreal lon_Deg;
	qreal altitude_MSL_Ft;
	qreal altitude_AGL_Ft;
	qreal verticalSpeed_FtMin;
	qreal vCAS_Kt;
	qreal vGS_Kt;
};
#pragma pack()

class Aircraft : public QObject
{
public:
	explicit Aircraft(const QString &callsign, QObject *parent = Q_NULLPTR);

	int type() const;

	LatLon position_Deg() const;
	void setPosition(const LatLon &pos_Deg);

	qreal altitudeMSL_Ft() const;
	void setAltitudeMSL(const qreal &altMSL_Ft);

	qreal altitudeAGL_Ft() const;
	void setAltitudeAGL(const qreal &altAGL_Ft);

	qreal verticalSpeed_FtMin() const;
	void setVerticalSpeed(const qreal &verticalSpeed_FtMin);

	qreal vCAS_Kt() const;
	void setVCAS(const qreal &vCAS_Kt);

	qreal vGS_Kt() const;
	void setVGS(const qreal &vGS_Kt);

	QString callsign() const;

	void setAircraftData(const AircraftData &aircraftData);

private:
	AircraftData m_aircraftData;
	const QString m_callsign;

signals:

public slots:

};

} // namespace Data

#endif // AIRCRAFT_H
