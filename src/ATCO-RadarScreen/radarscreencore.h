#ifndef RADARSCREENCORE_H
#define RADARSCREENCORE_H

#include <QObject>
#include <QSharedPointer>
#include <QWeakPointer>
#include <QMap>
#include "plugininterface_global.h"
#include "projectionabstract.h"
#include "radarscreenscene.h"
#include "radarscreenplugin.h"
#include "settings.h"

class PLUGININTERFACESHARED_EXPORT RadarScreenCore
{
public:
	static RadarScreenCore &inst();

	void loadPlugins();

	ProjectionAbstract &mapProjection();
	RadarScreenScene &scene();
	Settings &settings();

	void setMapProjection(const QSharedPointer<ProjectionAbstract> &mapProjection);

private:
	RadarScreenCore();


	QSharedPointer<ProjectionAbstract> m_mapProjection;
	QSharedPointer<RadarScreenScene> m_scene;
	QSharedPointer<Settings> m_settings;

	QMap<QString, QSharedPointer<RadarScreenPlugin> > m_registeredplugins;
};

#endif // RADARSCREENCORE_H
