#ifndef PROJECTIONLAMBERT_H
#define PROJECTIONLAMBERT_H

#include <QObject>
#include <QPointF>
#include <QRectF>
#include <QSharedPointer>

#include "projectionabstract.h"
#include "latlon.h"

const qreal DEFAULT_LAMBERT_LAT1 = 59;
const qreal DEFAULT_LAMBERT_LAT2 = 48;

namespace GeographicLib {
	class LambertConformalConic;
}

class PLUGININTERFACESHARED_EXPORT ProjectionLambert : public ProjectionAbstract
{
public:
	struct LambertParameter
	{
		LambertParameter():
			lat1_deg(DEFAULT_LAMBERT_LAT1),
			lat2_deg(DEFAULT_LAMBERT_LAT2){}
		LambertParameter(qreal lat1_deg, qreal lat2_deg) {
			this->lat1_deg = lat1_deg;
			this->lat2_deg = lat2_deg;
		}
		qreal lat1_deg;
		qreal lat2_deg;
	};

	explicit ProjectionLambert();

	QPointF convertToXYFromRad(LatLon latLon_rad);
	QPointF convertToXYFromDeg(LatLon latLon_deg);

	LatLon convertToLatLon_Deg(QPointF xy);
	LatLon convertToLatLon_Rad(QPointF xy);

	QRectF convertToXYRect(LatLon topLeftDeg, LatLon bottomRightDeg);

	LambertParameter lambertParameter() const;
	void setLambertParameter(const LambertParameter &lambertParameter);

	QPointF origin() const;
	void setOrigin(const QPointF &origin);

	qreal scale() const;
	void setScale(const qreal &scale, const QPointF &xy = QPointF());

	void operator *=(qreal zoomFactor);

private:

	QSharedPointer<GeographicLib::LambertConformalConic> m_geoLambertPtr;
	LambertParameter m_lambertParameter;

	QPointF m_origin;
	qreal m_scale;

};

#endif // PROJECTIONLAMBERT_H
